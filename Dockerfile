FROM ubuntu:20.04

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
    autoconf \
    make \
    build-essential \
    flex \
    texinfo \
    unzip \
    help2man \
    libtool-bin \
    libncurses-dev \
    bison \
    git \
    gawk \
    wget \
    libssl-dev \
    bc \
    cpio \
    rsync \
    sed  \
    binutils \
    gcc \
    g++ \
    bash \
    patch \
    gzip \
    bzip2 \
    perl \
    tar  \
    python

RUN useradd -ms /bin/bash newuser

USER newuser
ENV HOME /home/newuser
WORKDIR /home/newuser

RUN git clone https://github.com/crosstool-ng/crosstool-ng && \
    cd crosstool-ng && \
    git checkout crosstool-ng-1.24.0 && \
    ./bootstrap && \
    ./configure --enable-local && \
    make

COPY crosstool_ng_defconfig crosstool-ng/

RUN cd crosstool-ng && \
    wget https://github.com/libexpat/libexpat/releases/download/R_2_4_1/expat-2.4.1.tar.bz2 && \
    wget https://libisl.sourceforge.io/isl-0.20.tar.bz2 && \
    ./ct-ng defconfig DEFCONFIG=crosstool_ng_defconfig &&\
    ./ct-ng build && \
    ./ct-ng clean

ENV PATH="${HOME}/ISO_II/toolchain/arm-mse-linux-gnueabihf/bin:${PATH}"
