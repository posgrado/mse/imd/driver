# Driver

**Work in progress!**

## Description

## Prerequisites

Make sure you have installed all of the following prerequisites on your development machine:

- Git - [Download & Install Git](https://git-scm.com/downloads). Linux machines typically have this already installed.
- Ansible - [Download & Install Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Usage

### Local development

1. Clone this project `git clone https://gitlab.com/posgrado/mse/imd/driver.git`
1. Enter to the repository folder `cd driver`
1. Create a folder to share the root file system `sudo mkdir rootfs`
1. Set up the enviroment using ansible 

```sh
ansible-playbook dev_setup.yml -i inventory --extra-vars "ansible_user=${USER}"
```

4. Build the toolchain docker image `docker build . -t toolchain:latest`
5. Init the kernel and U-Boot submodules

```sh
git submodule init
git submodule update
```

6. Run a docker container and build U-Boot, kernel and Buildroot

```sh
docker run -it -v $PWD:/home/newuser/driver toolchain
export ARCH=arm
export CROSS_COMPILE=arm-linux-
cd driver/u-boot
make am335x_evm_defconfig
make
cd ..
cd linux
make omap2plus_defconfig
make
cd ..
cd buildroot
make defconfig BR2_DEFCONFIG=../br_defconfig
make
exit
```

```sh
cd ..
cp u-boot/MLO u-boot/u-boot.img /media/davidb/boot
```

```sh
sudo cp linux/arch/arm/boot/zImage /srv/tftp
cd rootfs
sudo tar -xvf ../buildroot/output/images/rootfs.tar
```

```
setenv autoload no
dhcp
setenv serverip 172.16.1.32
tftpboot 0x80000000 zImage
tftpboot 0x81000000 am335x-bonegreen.dtb
setenv bootargs root=/dev/nfs rw ip=172.16.1.36 console=ttyS0,115200n8 nfsroot=172.16.1.32:/home/davidb/Downloads/driver/rootfs,nfsvers=3
bootz 0x80000000 - 0x81000000
```

## Support

Please create a new issue if you need help.

## Roadmap

The main goal is to deliver a self contained repository. Clonning this project you should be able to develop, test and deliver the driver.

## License

This project is release under GNU GPLv2 license. Check it [here](LICENSE)

## Project status

Work in Progress, please check this repository frequently to find new features.

