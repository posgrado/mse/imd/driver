#include <linux/module.h>
#include <linux/fs.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/of.h>
#include <linux/i2c.h>

/* Private device structure */
struct mse_dev {
	struct i2c_client *client;
	struct miscdevice mse_miscdevice;
	char name[9]; /* msedrvXX */

};

/* User is reading data from /dev/msedrvXX */
static ssize_t mse_read(struct file *file, char __user *userbuf, size_t count, loff_t *ppos)  {

	struct mse_dev *mse;
	char buffer[21] = {0};
	int ret = 0;
	
	pr_info("mse_read() fue invocada. Con count = %d", count);
	
	mse = container_of(file->private_data, struct mse_dev, mse_miscdevice);

	ret = i2c_master_recv(mse->client, buffer, count);

	if (IS_ERR(&ret)) {
		pr_info("Error reading data for i2c = %d", ret);
		return ret;
	}

	ret = copy_to_user(userbuf, buffer, count);

	if (IS_ERR(&ret)) {
			pr_info("Error with copy_to_user with error = %d", ret);
			return ret;
	}
	
	return 0;
}

static ssize_t mse_write(struct file *file, const char __user *userbuf, size_t len, loff_t *offset)  {
	
	struct mse_dev *mse;
	char buffer[21] = {0};
	int ret = 0;
	
	pr_info("mse_write() fue invocada.");

	mse = container_of(file->private_data, struct mse_dev, mse_miscdevice);
		
	/* Aqui ira las llamadas a i2c_transfer() que correspondan pasando
	 * como dispositivo mse->client
	 *
	*/		
	ret = copy_from_user(buffer, userbuf, len);

	if (IS_ERR(&ret)) {
		pr_info("Error with copy_from_user with error = %d", ret);
		return ret;
	}

	ret = i2c_master_send(mse->client, buffer, len);	
	
	if (IS_ERR(&ret)) {
                pr_info("Error sending data for i2c = %d", ret);
                return ret;
	}

	return len;
}

static long mse_ioctl(struct file *file, unsigned int cmd, unsigned long arg)  {
	struct mse_dev *mse;
	
	mse = container_of(file->private_data, struct mse_dev, mse_miscdevice);
		
	/* Aqui ira las llamadas a i2c_transfer() que correspondan pasando
	 * como dispositivo mse->client
	 *
	*/
	
	pr_info("my_dev_ioctl() fue invocada. cmd = %d, arg = %ld\n", cmd, arg);
	return 0;
}

/* declaracion de una estructura del tipo file_operations */
static const struct file_operations mse_fops = {
	.owner = THIS_MODULE,
	.read = mse_read,
	.write = mse_write,
	.unlocked_ioctl = mse_ioctl,
};

/******************************************************************************
   *********** DEFINICION DE FUNCIONES PROBE() Y REMOVE() *********************
******************************************************************************/

/* Definicion de funcion probe() */
static int __init my_probe(struct i2c_client *client, const struct i2c_device_id *id)  {
	struct mse_dev * mse;
	static int counter = 0;
	int ret_val;
 
        pr_info("my_probe() function is called.\n");

	/* Allocate new private structure */
	mse = devm_kzalloc(&client->dev, sizeof(struct mse_dev), GFP_KERNEL);
	
	/* Store pointer to the device-structure in bus device context */
	i2c_set_clientdata(client,mse);
	
	/* Store pointer to I2C client device in the private structure */
	mse->client = client;
	
	/* Initialize the misc device, mse is incremented after each probe call */
	sprintf(mse->name, "mse%02d", counter++);
	
	mse->mse_miscdevice.name = mse->name;
	mse->mse_miscdevice.minor = MISC_DYNAMIC_MINOR;
	mse->mse_miscdevice.fops = &mse_fops;

        ret_val = misc_register(&mse->mse_miscdevice);
        if (ret_val != 0) {		
               pr_err("could not register the misc device mydev");
               return ret_val;
        }
        pr_info("Dispositivo %s: minor asignado: %i\n", mse->mse_miscdevice.name, mse->mse_miscdevice.minor);
        return 0;
};

/* Add remove() function */
static int __exit my_remove(struct i2c_client * client)  {
	struct mse_dev * mse;

        pr_info("my_remove() function is called.\n");
	/* Get device structure from bus device context */
	mse = i2c_get_clientdata(client);
        misc_deregister(&mse->mse_miscdevice);
        return 0;
};

/******************************************************************************
   ************************ OF MATCH STYLE ************************************
******************************************************************************/

/* Declare a list of devices supported by the driver */
static const struct of_device_id my_of_ids[] = {
        { .compatible = "mse,imd,paj7620", },
        {/*sentinel*/},
};
MODULE_DEVICE_TABLE(of, my_of_ids);


/******************************************************************************
   ***************** DECLARACION DE PLATFORM DRIVER ***************************
******************************************************************************/
/* Define platform driver structure */
static struct i2c_driver my_i2c_driver = {
        .probe = my_probe,
        .remove = my_remove,
        .driver = {
	        .name = "paj7620_driver",	
                .of_match_table = my_of_ids,
                .owner = THIS_MODULE,
        }
};

/* Register your platform driver */
module_i2c_driver(my_i2c_driver);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("David Mauricio Broin <davidmbroin@gmail.com>");
MODULE_DESCRIPTION("PAJ7620 Gesture Sensor Driver");